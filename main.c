/******************************************************************************
* PROGRAM: ASSIGNEMENT NO 5 - CHECKING PROGRAM FOR PARSING S-RECORD FILE
* CLASS: HN20_FR_EMB_04
* MADE BY: TUANKX
*******************************************************************************/


/*******************************************************************************
 * Includes
 ******************************************************************************/
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "srec.h"


/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define SREC_FILE_IN    "srec1.txt"
#define SREC_FILE_OUT   "Output.txt"
#define SREC_BUF_SIZE   80


/*******************************************************************************
 * Prototypes
 ******************************************************************************/

/* Function to check SREC File is error or not, return true if it's no error */
bool static SREC_Check_File(FILE* pFileIn,FILE* pFileOut, char* srecLine, uint8_t n);




/*******************************************************************************
 * Code
 ******************************************************************************/

int main()
{
    FILE* pFileIn;
    FILE* pFileOut;

    /* Declare a buffer for reading S-RECORD Line */
    char srecLine[SREC_BUF_SIZE];

    /* Declare a variable for receiving checking result of S-RECORD File*/
    bool checkStatus = false;

    pFileIn = fopen(SREC_FILE_IN, "r");

    pFileOut = fopen(SREC_FILE_OUT, "w+");

    if (!pFileIn)
    {
        printf("CAN NOT OPEN INPUT FILE");
    }
    else
    {
        checkStatus = SREC_Check_File(pFileIn, pFileOut, srecLine, SREC_BUF_SIZE);

        if (checkStatus)
        {
            printf("\nNO ERROR !");
        }
        else
        {
            fclose(pFileOut);
            SREC_Remove_File(SREC_FILE_OUT);
        }
    }

    fclose(pFileIn);
    fclose(pFileIn);
    printf("\n\n");

    return 0;
}

bool static SREC_Check_File(FILE* pFileIn, FILE* pFileOut, char* srecLine, uint8_t digitNumber)
{
    bool retVal = true;
    uint8_t lineCount = 0;

    printf("\n========================CHECKING RESULT========================\n");

    /* Loop to EOF for checking line by line*/
    while (!feof(pFileIn))
    {
        lineCount++;
        if (SREC_Read_Line(pFileIn, srecLine, digitNumber))
        {
                if (SREC_Check_Char(srecLine) == false)
                {
                    retVal = false;
                    printf("\nLine No %d: Character Error", lineCount);
                }

                if (SREC_Check_Byte_Count(srecLine) == false)
                {
                    retVal = false;
                    printf("\nLine No %d: Byte Count Error", lineCount);
                }

                if (SREC_Check_Checksum(srecLine) == false)
                {
                    retVal = false;
                    printf("\nLine No %d: Check Sum Error", lineCount);
                }
                if ((srecLine[1] == '5') || (srecLine[1] == '6'))
                {
                    if (SREC_Check_Line_Count(srecLine, lineCount - 2) == false)
                    {
                        retVal = false;
                        printf("\nLine No %d: Line Count Error", lineCount);
                    }
                }
        }

        /* If there's no error of line, write that line to a file */
        if (retVal == true)
        {
            if ((srecLine[1] == '1') || (srecLine[1] == '2') || (srecLine[1] == '3'))
            {
                SREC_Write_Line(srecLine, pFileOut);
            }
        }
    }

    return retVal;
}
