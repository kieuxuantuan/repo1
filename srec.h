/******************************************************************************
* PROGRAM: ASSIGNEMENT NO 5 - CHECKING PROGRAM FOR PARSING S-RECORD FILE
* CLASS: HN20_FR_EMB_04
* MADE BY: TUANKX
*******************************************************************************/


#ifndef _SREC_H_
#define _SREC_H_


/*******************************************************************************
 * Prototypes
 ******************************************************************************/

/* Function to read a SREC - line and put data to a string */
bool SREC_Read_Line(FILE* pFileIn, char* srecLine, uint8_t digitNumber);

/* Function to print a Line to the screen */
void SREC_Print_Line(char* srecLine);

/* Function to write a line to a text file */
void SREC_Write_Line(char* srecLine, FILE* pFileOut);

/* Function to check Character of a line */
bool SREC_Check_Char(char* srecLine);

/* Function to check Byte Count a line */
bool SREC_Check_Byte_Count(char* srecLine);

/* Function to check Checksum of a line */
bool SREC_Check_Checksum(char* srecLine);

/* Function to check Line Count of a Line */
bool SREC_Check_Line_Count(char* srecLine, uint8_t lineCount);

/* Function to remove a file */
void SREC_Remove_File(const char* pFile);


#endif /* _SREC_H_ */

