/******************************************************************************
* PROGRAM: ASSIGNEMENT NO 5 - CHECKING PROGRAM FOR PARSING S-RECORD FILE
* CLASS: HN20_FR_EMB_04
* MADE BY: TUANKX
*******************************************************************************/


/*******************************************************************************
 * Includes
 ******************************************************************************/
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include "srec.h"


/*******************************************************************************
 * Prototypes
 ******************************************************************************/
static char Hexa_To_Dec(char hexa);



/*******************************************************************************
 * Code
 ******************************************************************************/

bool SREC_Read_Line(FILE* pFileIn, char* srecLine, uint8_t digitNumber)
{
    bool retVal = true;
    char *str = fgets(srecLine, digitNumber, pFileIn);

    if (str == NULL)
    {
        retVal = false;
    }

    return retVal;
}

void SREC_Print_Line(char* srecLine)
{
    printf("%s", srecLine);
}


char Hexa_To_Dec(char hexa)
{
    char retVal = 0;

    if ('0' <= hexa && hexa <= '9')
    {
        retVal = hexa - '0';
    }

    if ('A' <= hexa && hexa <= 'F')
    {
        retVal = (hexa - 'A') + 10;
    }

    return retVal;
}

bool SREC_Check_Char(char* srecLine)
{
    bool retVal = true;
    uint8_t index = 0;

    while (srecLine[index] != '\n')
    {
        /* Check S character at the begin of line*/
        if ((index == 0) && (srecLine[0] != 'S'))
        {
            retVal = false;
        }

        /* Check Record-Type of line*/
        if ((index == 1) && (srecLine[1] == '4'))
        {
            retVal = false;
        }

        /* Check remaining Digit of line*/
        if (index > 1)
        {
            if ((srecLine[index] < '0') || ('F' < srecLine[index])
            || ('9' < srecLine[index] && srecLine[index] < 'A'))
            {
                retVal = false;
            }
        }

        index++;
    }

    return retVal;
}

bool SREC_Check_Checksum(char* line)
{
    bool retVal = false;
    uint8_t digitSum = 0;
    uint8_t index = 0;
    uint8_t digitLength = 0;

    /* digitLength variable is the length of hex digit in a line,
    digitLength = total length - 1 ('\n' at the end of line) */
    digitLength = strlen(line) - 1;

    for (index = 2; index < (digitLength); index += 2)
    {
        digitSum = digitSum + Hexa_To_Dec(line[index]) * 16 + Hexa_To_Dec(line[index+1]);
    }

    if ((digitSum & 0xFF) == 0xFF)
    {
        retVal = true;
    }

    return retVal;
}

bool SREC_Check_Byte_Count(char* srecLine)
{
    bool retVal = false;
    uint8_t byteCount = 0;
    uint8_t countNumber = 0;

    byteCount = Hexa_To_Dec(srecLine[2]) * 16 + Hexa_To_Dec(srecLine[3]);

    /* countNumber = total length - 4(Record type + Byte Count) - 1 ('\n') */
    countNumber = strlen(srecLine) - 4 - 1;

    if (byteCount * 2 == countNumber)
    {
        retVal = true;
    }

    return retVal;
}


bool SREC_Check_Line_Count(char* srecLine, uint8_t lineCount)
{
    uint8_t index;
    bool retVal = false;
    uint8_t sum = 0;
    uint8_t byteCount = Hexa_To_Dec(srecLine[2]) * 16 + Hexa_To_Dec(srecLine[3]);

    /* Declare addressDigit variable is total address digit SREC Line
    addressDigit = ByteCount digit(4) - CheckSum digit (2) */
    uint8_t addressDigit = byteCount * 2 - 2;

    /* Calculate the value of address field (line count)*/
    for (index = 4; index < addressDigit + 4; index++)
    {
        sum = sum * 16 + Hexa_To_Dec(srecLine[index]);
    }

    if (sum == lineCount)
    {
        retVal = true;
    }

    return retVal;
}

void SREC_Write_Line(char* srecLine, FILE* pFileOut)
{
    uint8_t byteCount = 0;
    uint8_t addressDigit = 0;
    uint8_t dataDigit = 0;
    uint8_t index = 0;

    if (srecLine[1] == '1')
    {
        addressDigit = 4;
    }
    if (srecLine[1] == '2')
    {
        addressDigit = 6;
    }
    if (srecLine[1] == '3')
    {
        addressDigit = 8;
    }

    byteCount = Hexa_To_Dec(srecLine[2]) * 16 + Hexa_To_Dec(srecLine[3]);

    /* addressDigit = ByteCount digits - addressDigit - Checksum digit (2) */
    dataDigit = byteCount * 2 - addressDigit - 2;

    /* Print the address digit to a line of a file */
    for (index = 4; index < addressDigit + 4; index++)
    {
        fputc(srecLine[index], pFileOut);
    }

    fputc(' ', pFileOut);

    /* Print the data digit to a line of a file */
    for (index = addressDigit+4; index < dataDigit + addressDigit+4; index++)
    {
        fputc(srecLine[index], pFileOut);
    }

    fputc('\n', pFileOut);
}

void SREC_Remove_File(const char *pFile)
{
    remove(pFile);
}


