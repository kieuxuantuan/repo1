#!/usr/bin/env groovy

 

// This is the script for a Jenkins pipeline - also known as a "Jenkinsfile"

// which is used to run a build under a jenkins pipeline project.

 

// This specific file builds the Linux host SDK.

 

pipeline {
    agent none
    options { buildDiscarder(logRotator(numToKeepStr: "10")) }
    stages {
        stage('Build x86_64') {
            agent { label 'Tuan-VituralUbuntu' }
            steps {    
                        sh '''
                        cd repo1
                        python3 test.py
                        
                        chmod a+x TuanKX_Assignment5.exe
                        
                        rm -rf Deploy
                        mkdir Deploy
                        cd Deploy
                        cd ..
                        '''
                        archiveArtifacts artifacts: 'repo1/test.py'
                }
                post {
                        success {
                            sh ' echo "x86_64 build success." '
                           
                        }
                        failure {
                            sh ' echo "x86_64 build failed." '
                        }
                        unstable {
                            sh ' echo "x86_64 build unstable." '
                        }
                    }
                }
    }

 

    post {
        success {
            script {
                echo "Success happened."
            }
        }
    }

}